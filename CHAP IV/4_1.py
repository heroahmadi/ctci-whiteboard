# Breath First Search

from collections import defaultdict

class Graph:
	def __init__(self):
		self.node = defaultdict(list)

	def push(self, ind, val):
		self.node[ind].append(val)

	def BFS(self, start, dest):
		visited = [False]*(len(self.node))
		queue = []

		queue.append(start)
		visited[start] = True

		while queue:
			current = queue.pop(0)

			if current == dest:
				return True

			for i in self.node[current]:
				if visited[i] == False:
					queue.append(i)
					visited[i] = True

		return False

g = Graph() 
g.push(0, 1) 
g.push(0, 2) 
g.push(1, 2) 
g.push(2, 0) 
g.push(2, 3) 
g.push(3, 3) 

start = int(raw_input('Starting node: '))
dest = int(raw_input('Destination node: '))
print g.BFS(start, dest)