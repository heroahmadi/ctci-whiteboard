# Chapter I

## - Global Linked List Class
```
 class doubleLinkedList():
 	def __init__(self, val):
		self.val = val
		self.next = NULL
		self.prev = NULL

	def deleteNode():
		self.prev.next = self.next
		self.next.prev = self.prev

class singleLinkedList():
 	def __init__(self, val):
		self.val = val
		self.next = NULL

	def deleteNode():
		self.prev.next = self.next
```

## 2.1. Remove Dups
```
 def removeDups(nodes):
 	hasht = {}
	while nodes.next:
		if nodes.val in hasht:
			nodes.deleteNode()
		else:
			hasht[nodes.val] = 1

		nodes = nodes.next
```

## 2.2. Return Kth to Last
```
 def removeDups(nodes):
 	hasht = {}
	while nodes.next:
		if nodes.val in hasht:
			nodes.deleteNode()
		else:
			hasht[nodes.val] = 1
			
		nodes = nodes.next
```