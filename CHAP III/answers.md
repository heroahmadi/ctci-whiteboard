# Chapter III

## 3.2. Stack Min
	By declaring 2 new private properties for class Stack, called min and max. Min stores pointer to stack node which has the minimum value of the stack. And max stores pointer to stack node which has the maximum value of the stack. And we also need a new private property in stack node. This property stores pointer to another node which has the biggest value but still less than its own node value. This is the pseudocode for the stack class:

```
	class Stack():
		def __init__(self):
			self.min = null
			self.top = null

	class StackNode(Stack):
		def __init__(self, data):
			self.data = data
			self.next = null
			self.sub_min = null

	def pop:
		self.min = top.sub_min
		top = self.next

	def push(data):
		node = StackNode(data)
		if(self.min == null):
			self.min = node
			node.sub_min = node
		elif(node.data < self.min.data):
			node.sub_min = node
			self.min = node
		else:
			node.sub_min = self.min
```

## 3.3 Stack of Plates

```
	class SetOfStacks():
		def __init__(self):
			self.top = null
			self.max = 10

		class Stack():
			def __init__(self):
				self.top = null
				self.next = null
				self.count = 0

			class StackNode:
				def __init__(self, data):
					self.next = null
					self.data = data

			def push(data):
				node = StackNode(data)
				node.next = top
				top = node
				self.count += 1

			def pop:
				if top == null:
					return 'error'
				val = top.data
				top = top.next
				self.count -= 1
				return val

			def stackCount:
				return self.count
		
		def push(data):
			if self.top == null:
				stack = Stack
				self.top = stack
			if self.top.stackCount >= max:
				stack = Stack
				self.top.next = stack
				self.top = stack
			else:
				stack = top

			stack.push(data)
		
		def pop:
			if self.top == null:
				return 'error'
			val = self.top.pop
			if self.top.stackCount == 0:
				self.top = self.top.next
```